import moment from 'moment';

// Get the dates array in a given unit within a date range
export const getDatesArray = (from, to, unit = 'month') => {
  const startDate = moment.utc(from).startOf(unit);
  const endDate = moment.utc(to).endOf(unit);
  const dateFormat = unit === 'month' ? 'YYYY-MM' : 'YYYY';

  const dates = [];
  const date = moment.utc(startDate); // clone the startDate

  // eslint-disable-next-line no-unmodified-loop-condition
  while (date < endDate) {
    dates.push(date.format(dateFormat));
    date.add(1, unit);
  }

  return dates;
};

// Get the start and end dates for a given dateString (YYYY-MM or YYYY)
export const getStartAndEndOfDate = (dateString) => {
  const [year, month] = dateString.split('-');
  if (year && month) {
    const m = moment({ year, month }).utc();
    return [m.startOf('month').toDate(), m.endOf('month').toDate()];
  } else {
    const m = moment({ year }).utc();
    return [m.startOf('year').toDate(), m.endOf('year').toDate()];
  }
};
