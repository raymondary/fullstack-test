import passport from 'passport';
import passportJWT from 'passport-jwt';
import Config from '@config';

const JwtStrategy = passportJWT.Strategy;
const ExtractJWT = passportJWT.ExtractJwt;

export const setupPassport = () => {
  const jwtOptions = {
    jwtFromRequest: ExtractJWT.fromExtractors([
      ExtractJWT.fromAuthHeaderWithScheme('Bearer'),
      ExtractJWT.fromUrlQueryParameter('accessToken'),
    ]),
    secretOrKey: Config.JWT_SECRET,
  };

  passport.use(
    new JwtStrategy(jwtOptions, async (token, done) => {
      try {
        return done(null, token.user);
      } catch (error) {
        done(error);
      }
    })
  );
};
