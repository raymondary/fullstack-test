import Joi from 'joi';
import passport from 'passport';
import { login, signup, getCurrentUser } from '@controllers';
import { validate } from '@libs';

const LoginSchema = Joi.object({
  email: Joi.string().required().email(),
  password: Joi.string().required(),
});

const SignupSchema = Joi.object({
  firstName: Joi.string().required(),
  lastName: Joi.string().required(),
  email: Joi.string().required().email(),
  password: Joi.string().required(),
});

export const configRoutes = (router) => {
  router.get(
    '/user/data',
    passport.authenticate('jwt', { session: false }),
    getCurrentUser
  );
  router.post('/user/login', validate(LoginSchema, 'body'), login);
  router.post('/user/signup', validate(SignupSchema, 'body'), signup);
};
