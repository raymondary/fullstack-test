import express from 'express';
import { configRoutes as configUserRoutes } from './user';
import { configRoutes as configOrgRoutes } from './org';
import { configRoutes as configDepartmentRoutes } from './department';
import { configRoutes as configEmployeeRoutes } from './employee';
import { configRoutes as configAnalyticsRoutes } from './analytics';

export const configureRoutes = (app) => {
  const apiRouter = express.Router();
  configUserRoutes(apiRouter);
  configOrgRoutes(apiRouter);
  configDepartmentRoutes(apiRouter);
  configEmployeeRoutes(apiRouter);
  configAnalyticsRoutes(apiRouter);
  app.use('/api', apiRouter);
};
