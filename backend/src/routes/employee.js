import Joi from 'joi';
import passport from 'passport';
import {
  listEmployees,
  createEmployee,
  getEmployeeById,
  updateEmployeeById,
  deleteEmployeeById,
} from '@controllers';
import { validate } from '@libs';

const CreateEmployeeSchema = Joi.object({
  firstName: Joi.string().required(),
  lastName: Joi.string().required(),
  dob: Joi.date().required(),
  workTitle: Joi.string().required(),
  totalExperience: Joi.number().required(),
  org: Joi.string().required(),
  department: Joi.string().required(),
});

const UpdateEmployeeSchema = Joi.object({
  firstName: Joi.string(),
  lastName: Joi.string(),
  dob: Joi.date(),
  workTitle: Joi.string(),
  totalExperience: Joi.number(),
  department: Joi.string(),
});

export const configRoutes = (router) => {
  const authMiddleware = passport.authenticate('jwt', { session: false });

  router.get('/employee', authMiddleware, listEmployees);
  router.post(
    '/employee',
    [authMiddleware, validate(CreateEmployeeSchema, 'body')],
    createEmployee
  );
  router.get('/employee/:id', authMiddleware, getEmployeeById);
  router.put(
    '/employee/:id',
    [authMiddleware, validate(UpdateEmployeeSchema, 'body')],
    updateEmployeeById
  );
  router.delete('/employee/:id', authMiddleware, deleteEmployeeById);
};
