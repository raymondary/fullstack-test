import Joi from 'joi';
import passport from 'passport';
import {
  listDepartments,
  createDepartment,
  getDepartmentById,
  updateDepartmentById,
  deleteDepartmentById,
} from '@controllers';
import { validate } from '@libs';

const CreateDepartmentSchema = Joi.object({
  name: Joi.string().required(),
  description: Joi.string().required(),
  org: Joi.string().required(),
  workingTime: Joi.array()
    .items(
      Joi.object({
        from: Joi.string(),
        to: Joi.string(),
      })
    )
    .required(),
  workingDays: Joi.array().items(Joi.string()).required(),
});

const UpdateDepartmentSchema = Joi.object({
  name: Joi.string(),
  description: Joi.string(),
  workingTime: Joi.array().items(
    Joi.object({
      from: Joi.string(),
      to: Joi.string(),
    })
  ),
  workingDays: Joi.array().items(Joi.string()),
});

export const configRoutes = (router) => {
  const authMiddleware = passport.authenticate('jwt', { session: false });

  router.get(
    '/department',
    [authMiddleware, validate(CreateDepartmentSchema, 'body')],
    listDepartments
  );
  router.post('/department', authMiddleware, createDepartment);
  router.get('/department/:id', authMiddleware, getDepartmentById);
  router.put(
    '/department/:id',
    [authMiddleware, validate(UpdateDepartmentSchema, 'body')],
    updateDepartmentById
  );
  router.delete('/department/:id', authMiddleware, deleteDepartmentById);
};
