import Joi from 'joi';
import passport from 'passport';
import { getAnalyticsStatus } from '@controllers';
import { validate } from '@libs';

const GetAnalyticsStatusSchema = Joi.object({
  type: Joi.string().valid('organization', 'department', 'employee').required(),
  groupBy: Joi.string().valid('month', 'year').required(),
  from: Joi.date(),
  to: Joi.string(),
});

export const configRoutes = (router) => {
  const authMiddleware = passport.authenticate('jwt', { session: false });

  router.get(
    '/analytics/status',
    [authMiddleware, validate(GetAnalyticsStatusSchema, 'query')],
    getAnalyticsStatus
  );
};
