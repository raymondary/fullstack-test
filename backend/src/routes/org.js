import Joi from 'joi';
import passport from 'passport';
import {
  listOrg,
  createOrg,
  getOrgById,
  updateOrgById,
  deleteOrgById,
} from '@controllers';
import { validate } from '@libs';

const CreateOrgSchema = Joi.object({
  name: Joi.string().required(),
  address: Joi.string().required(),
  city: Joi.string().required(),
  state: Joi.string(),
  country: Joi.string().required(),
});

const UpdateOrgSchema = Joi.object({
  name: Joi.string(),
  address: Joi.string(),
  city: Joi.string(),
  state: Joi.string(),
  country: Joi.string(),
});

export const configRoutes = (router) => {
  const authMiddleware = passport.authenticate('jwt', { session: false });

  router.get('/org', authMiddleware, listOrg);
  router.post(
    '/org',
    [authMiddleware, validate(CreateOrgSchema, 'body')],
    createOrg
  );
  router.get('/org/:id', authMiddleware, getOrgById);
  router.put(
    '/org/:id',
    [authMiddleware, validate(UpdateOrgSchema, 'body')],
    updateOrgById
  );
  router.delete('/org/:id', authMiddleware, deleteOrgById);
};
