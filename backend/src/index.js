import colors from 'colors';
import { connectDb } from '@models';
import { setupLogger } from '@libs';
import config from '@config';

colors.enable();
setupLogger();

connectDb(config.DATABASE).then(async () => {
  console.info(`Connected to the mongo database at ${config.DATABASE}!`);
  require('./app');
});
