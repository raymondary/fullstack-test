import mongoose from 'mongoose';
import { v4 as uuidv4 } from 'uuid';

const orgSchema = new mongoose.Schema(
  {
    _id: {
      type: String,
      default: () => uuidv4(),
    },

    name: {
      type: String,
      required: true,
    },

    owner: {
      type: String,
      ref: 'User',
    },

    address: {
      type: String,
      required: true,
    },

    city: {
      type: String,
      required: true,
    },

    state: {
      type: String,
    },

    country: {
      type: String,
      required: true,
    },
  },
  {
    timestamps: true,
  }
);

export default mongoose.model('Organization', orgSchema);
