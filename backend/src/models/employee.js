import mongoose from 'mongoose';
import { v4 as uuidv4 } from 'uuid';

const employeeSchema = new mongoose.Schema(
  {
    _id: {
      type: String,
      default: () => uuidv4(),
    },

    firstName: {
      type: String,
      required: true,
    },

    lastName: {
      type: String,
      required: true,
    },

    dob: {
      type: Date,
      required: true,
    },

    workTitle: {
      type: String,
      required: true,
    },

    totalExperience: {
      type: Number,
      required: true,
    },

    owner: {
      type: String,
      ref: 'User',
      required: true,
    },

    org: {
      type: String,
      ref: 'Organization',
      required: true,
    },

    department: {
      type: String,
      ref: 'Department',
      required: true,
    },
  },
  {
    timestamps: true,
  }
);

export default mongoose.model('Employee', employeeSchema);
