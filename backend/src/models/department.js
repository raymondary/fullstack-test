import mongoose from 'mongoose';
import { v4 as uuidv4 } from 'uuid';

const departmentSchema = new mongoose.Schema(
  {
    _id: {
      type: String,
      default: () => uuidv4(),
    },

    name: {
      type: String,
      required: true,
    },

    description: {
      type: String,
    },

    owner: {
      type: String,
      ref: 'User',
      required: true,
    },

    org: {
      type: String,
      ref: 'Organization',
      required: true,
    },

    workingTime: [
      {
        _id: false,
        from: String,
        to: String,
      },
    ],

    workingDays: [String],
  },
  {
    timestamps: true,
  }
);

export default mongoose.model('Department', departmentSchema);
