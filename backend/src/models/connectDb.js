import mongoose from 'mongoose';

export const connectDb = async (mongodbUrl) => {
  try {
    await mongoose.connect(mongodbUrl, {
      useCreateIndex: true,
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useFindAndModify: false,
    });
  } catch (err) {
    console.error(`DB Connection Error: ${err.message}`);
    throw err;
  }
};
