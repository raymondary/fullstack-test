import { EmployeeModel } from '@models';

/**
 * List all employees for the current user in a given criteria
 * GET /employee
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
export const listEmployees = async (req, res, next) => {
  try {
    const { _id: userId } = req.user;
    const { org, department } = req.query;

    const conditions = {
      owner: userId,
    };
    // Check if org query param is added
    if (org) {
      conditions.org = org;
    }
    // Check if department query param is added
    if (department) {
      conditions.department = department;
    }

    const total = await EmployeeModel.countDocuments();
    const employees = await EmployeeModel.find(conditions);

    res.status(200).json({
      message: 'Successfully list your employees',
      data: { total, employees },
    });
  } catch (error) {
    next(error);
  }
};

/**
 * Create a new employee
 * POST /employee
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
export const createEmployee = async (req, res, next) => {
  try {
    const { _id } = req.user;
    const {
      firstName,
      lastName,
      dob,
      workTitle,
      totalExperience,
      org,
      department,
    } = req.body;

    const employee = await EmployeeModel.create({
      firstName,
      lastName,
      dob,
      workTitle,
      totalExperience,
      owner: _id,
      org,
      department,
    });

    res.status(200).json({
      message: 'Successfully created a new employee',
      data: { employee },
    });
  } catch (error) {
    next(error);
  }
};

/**
 * Get employee by id
 * GET /employee/:id
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
export const getEmployeeById = async (req, res, next) => {
  try {
    const { _id: userId } = req.user;
    const { id } = req.params;
    const employee = await EmployeeModel.findById(id);

    if (!employee) {
      throw new Error('No employee is found');
    }
    if (employee.owner !== userId) {
      throw new Error('Not allowed to get this employee');
    }

    res.status(200).json({
      message: 'Successfully got the employee',
      data: { employee },
    });
  } catch (error) {
    next(error);
  }
};

/**
 * Delete employee by id
 * DELETE /employee/:id
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
export const deleteEmployeeById = async (req, res, next) => {
  try {
    const { _id: userId } = req.user;
    const { id } = req.params;
    const employee = await EmployeeModel.findById(id);

    if (!employee) {
      throw new Error('No employee is found');
    }
    if (employee.owner !== userId) {
      throw new Error('Not allowed to delete this employee');
    }

    await EmployeeModel.deleteOne({ _id: id });
    res.status(200).json({
      message: 'Successfully deleted the employee',
      data: {},
    });
  } catch (error) {
    next(error);
  }
};

/**
 * Update employee by id
 * PUT /employee/:id
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
export const updateEmployeeById = async (req, res, next) => {
  try {
    const { _id: userId } = req.user;
    const { id } = req.params;
    const payload = req.body;

    const employee = await EmployeeModel.findById(id);

    if (!employee) {
      throw new Error('No employee is found');
    }
    if (employee.owner !== userId) {
      throw new Error('Not allowed to delete this employee');
    }

    const updatedEmployee = await EmployeeModel.findOneAndUpdate(
      { _id: id },
      payload,
      {
        new: true,
      }
    );
    res.status(200).json({
      message: 'Successfully updated the employee',
      data: { employee: updatedEmployee },
    });
  } catch (error) {
    next(error);
  }
};
