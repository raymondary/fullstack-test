import moment from 'moment';
import jwt from 'jsonwebtoken';
import { UserModel } from '@models';
import Config from '@config';

/**
 * Log in with email and password
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
export const login = async (req, res, next) => {
  const { email, password } = req.body;

  try {
    const user = await UserModel.findOne({ email });
    if (!user) {
      throw new Error('User not found');
    }

    const isValid = await user.validatePassword(password);
    if (!isValid) {
      throw new Error('Wrong password');
    }

    // Set lastLogin timestamp
    user.lastLogin = moment.utc().toDate();
    await user.save();

    // Generate jwt token
    const body = { _id: user._id, email: user.email };
    const token = jwt.sign({ user: body }, Config.JWT_SECRET);

    res.status(200).json({
      message: 'Successfully logged in',
      data: { token },
    });
  } catch (error) {
    next(error);
  }
};

/**
 * Sign up a new user
 * @param {s} req
 * @param {*} res
 * @param {*} next
 */
export const signup = async (req, res, next) => {
  const { firstName, lastName, email, password } = req.body;

  try {
    const passwordHash = await UserModel.generatePasswordHash(password);

    const user = await UserModel.create({
      firstName,
      lastName,
      email,
      password: passwordHash,
    });
    res.status(200).json({
      message: 'Successfully signed up',
      data: { user },
    });
  } catch (error) {
    next(error);
  }
};

/**
 * Return data for the current user
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
export const getCurrentUser = async (req, res, next) => {
  try {
    const { _id } = req.user;
    const user = await UserModel.findById(_id);

    res.status(200).json({
      message: 'Success',
      data: { user },
    });
  } catch (error) {
    next(error);
  }
};
