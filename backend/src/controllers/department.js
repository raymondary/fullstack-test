import { DepartmentModel } from '@models';

/**
 * List all departments for the current user in a given criteria
 * GET /department
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
export const listDepartments = async (req, res, next) => {
  try {
    const { _id: userId } = req.user;
    const { org } = req.query;

    const conditions = {
      owner: userId,
    };
    // Check if org query param is added
    if (org) {
      conditions.org = org;
    }

    const total = await DepartmentModel.countDocuments();
    const departments = await DepartmentModel.find(conditions);

    res.status(200).json({
      message: 'Successfully list your departments',
      data: { total, departments },
    });
  } catch (error) {
    next(error);
  }
};

/**
 * Create a new department
 * POST /department
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
export const createDepartment = async (req, res, next) => {
  try {
    const { _id } = req.user;
    const { name, description = '', org, workingTime, workingDays } = req.body;

    const department = await DepartmentModel.create({
      name,
      description,
      owner: _id,
      org,
      workingTime,
      workingDays,
    });

    res.status(200).json({
      message: 'Successfully created a new department',
      data: { department },
    });
  } catch (error) {
    next(error);
  }
};

/**
 * Get department by id
 * GET /department/:id
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
export const getDepartmentById = async (req, res, next) => {
  try {
    const { _id: userId } = req.user;
    const { id } = req.params;
    const department = await DepartmentModel.findById(id);

    if (!department) {
      throw new Error('No department is found');
    }
    if (department.owner !== userId) {
      throw new Error('Not allowed to get this department');
    }

    res.status(200).json({
      message: 'Successfully got the department',
      data: { department },
    });
  } catch (error) {
    next(error);
  }
};

/**
 * Delete department by id
 * DELETE /department/:id
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
export const deleteDepartmentById = async (req, res, next) => {
  try {
    const { _id: userId } = req.user;
    const { id } = req.params;
    const department = await DepartmentModel.findById(id);

    if (!department) {
      throw new Error('No department is found');
    }
    if (department.owner !== userId) {
      throw new Error('Not allowed to delete this department');
    }

    await DepartmentModel.deleteOne({ _id: id });
    res.status(200).json({
      message: 'Successfully deleted the department',
      data: {},
    });
  } catch (error) {
    next(error);
  }
};

/**
 * Update department by id
 * PUT /department/:id
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
export const updateDepartmentById = async (req, res, next) => {
  try {
    const { _id: userId } = req.user;
    const { id } = req.params;
    const payload = req.body;

    const department = await DepartmentModel.findById(id);

    if (!department) {
      throw new Error('No department is found');
    }
    if (department.owner !== userId) {
      throw new Error('Not allowed to delete this department');
    }

    const updatedDepartment = await DepartmentModel.findOneAndUpdate(
      { _id: id },
      payload,
      {
        new: true,
      }
    );
    res.status(200).json({
      message: 'Successfully updated the department',
      data: { department: updatedDepartment },
    });
  } catch (error) {
    next(error);
  }
};
