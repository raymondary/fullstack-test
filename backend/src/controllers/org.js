import { OrgModel } from '@models';

/**
 * List all organizations
 * GET /org
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
export const listOrg = async (req, res, next) => {
  try {
    const { _id } = req.user;
    const total = await OrgModel.countDocuments();
    const orgs = await OrgModel.find({ owner: _id });

    res.status(200).json({
      message: 'Successfully list all of your organizations',
      data: { total, orgs },
    });
  } catch (error) {
    next(error);
  }
};

/**
 * Create a new organization
 * POST /org
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
export const createOrg = async (req, res, next) => {
  try {
    const { _id } = req.user;
    const { name, address, city, state, country } = req.body;

    const org = await OrgModel.create({
      name,
      owner: _id,
      address,
      city,
      state,
      country,
    });
    res.status(200).json({
      message: 'Successfully created a new organization',
      data: { org },
    });
  } catch (error) {
    console.log('error: ', error);
    next(error);
  }
};

/**
 * Get organization by id
 * GET /org/:id
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
export const getOrgById = async (req, res, next) => {
  try {
    const { _id: userId } = req.user;
    const { id } = req.params;
    const org = await OrgModel.findById(id);

    if (!org) {
      throw new Error('No organization is found');
    }
    if (org.owner !== userId) {
      throw new Error('Not allowed to get this organization');
    }

    res.status(200).json({
      message: 'Successfully got the organization',
      data: { org },
    });
  } catch (error) {
    next(error);
  }
};

/**
 * Delete organization by id
 * DELETE /org/:id
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
export const deleteOrgById = async (req, res, next) => {
  try {
    const { _id: userId } = req.user;
    const { id } = req.params;
    const org = await OrgModel.findById(id);

    if (!org) {
      throw new Error('No organization is found');
    }
    if (org.owner !== userId) {
      throw new Error('Not allowed to delete this organization');
    }

    await OrgModel.deleteOne({ _id: id });
    res.status(200).json({
      message: 'Successfully deleted the organization',
      data: {},
    });
  } catch (error) {
    next(error);
  }
};

/**
 * Update organization by id
 * PUT /org/:id
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
export const updateOrgById = async (req, res, next) => {
  try {
    const { _id: userId } = req.user;
    const { id } = req.params;
    const payload = req.body;

    const org = await OrgModel.findById(id);
    if (!org) {
      throw new Error('No organization is found');
    }
    if (org.owner !== userId) {
      throw new Error('Not allowed to delete this organization');
    }

    const updatedOrg = await OrgModel.findOneAndUpdate({ _id: id }, payload, {
      new: true,
    });

    res.status(200).json({
      message: 'Successfully updated the organization',
      data: { org: updatedOrg },
    });
  } catch (error) {
    next(error);
  }
};
