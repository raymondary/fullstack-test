import { OrgModel, DepartmentModel, EmployeeModel } from '@models';
import { getDatesArray, getStartAndEndOfDate } from '@utils';

export const getAnalyticsStatus = async (req, res, next) => {
  try {
    const { _id: userId } = req.user;
    const { type, groupBy, from, to } = req.query;

    const conditions = {
      owner: userId,
      createdAt: {},
    };

    if (from) {
      conditions.createdAt = {
        ...conditions.createdAt,
        $gte: from,
      };
    }
    if (to) {
      conditions.createdAt = {
        ...conditions.createdAt,
        $lte: to,
      };
    }

    // Prevent IIFE
    const ModelKlass = (() => {
      switch (type) {
        case 'organization':
          return OrgModel;
        case 'department':
          return DepartmentModel;
        case 'employee':
        default:
          return EmployeeModel;
      }
    })();

    const documents = await ModelKlass.find(conditions).sort({ createdAt: 1 });

    // Prepare analytics data to be used in chart
    const datesArray = getDatesArray(
      from || documents[0].createdAt,
      to || documents[documents.length - 1].createdAt,
      groupBy
    );
    const analytics = datesArray.map((date) => {
      const [start, end] = getStartAndEndOfDate(date);
      const count = documents.filter(
        (doc) => doc.createdAt >= start && doc.createdAt < end
      ).length;
      return { [date]: count };
    });
    res.status(200).json({
      message: 'Successfully got the analytics status',
      data: { analytics },
    });
  } catch (error) {
    next(error);
  }
};
