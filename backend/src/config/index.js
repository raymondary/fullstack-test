import { config } from 'dotenv';

// load .env file into process.env
config();

export default {
  NODE_ENV: process.env.NODE_ENV || 'development',
  DATABASE: process.env.DATABASE || 'mongodb://localhost:27017/resync',
  PORT: process.env.PORT || 3000,
  JWT_SECRET: process.env.JWT_SECRET || 'resync_jwt_secret',
};
