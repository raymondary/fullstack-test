import express from 'express';
import helmet from 'helmet';
import morgan from 'morgan';
import cors from 'cors';
import passport from 'passport';
import { configureRoutes } from '@routes';
import { catchAll, notFound, setupPassport } from '@libs';
import config from '@config';

const app = express();

app.use(helmet());
app.use(cors());
app.use(morgan('tiny'));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(passport.initialize());

app.get('/', (req, res) => {
  res.json({ message: 'Resync is up!' });
});

// Setup passport
setupPassport();

// Configure api routes
configureRoutes(app);

// Error handling middlewares
app.use(notFound);
app.use(catchAll);

app.listen(config.PORT, () => {
  console.info(`Server is ready at port ${config.PORT}`);
});
