# resync full-stack test backend 

> A Node.js backend with Express, MongoDB, Docker

# Development

* Installing dependencies

```bash
$ npm install
```

* Running scripts

| Action                    | Usage          |
| ------------------------- | -------------- |
| Starting development mode | `npm run dev`  |
| Linting code              | `npm run lint` |

# Docker

* Building an image

```bash
$ docker-compose build
```

* Running a container

```bash
$ docker-compose up
```

* Stopping a container

```bash
$ docker-compose down
```

# Postman

* postman/Resync Test Backend.postman_collection.json
* [Published Documentation](https://documenter.getpostman.com/view/13286296/TzkyP1U5)
