module.exports = function (api) {
  api.cache(true);

  const presets = [
    [
      '@babel/preset-env',
      {
        targets: {
          esmodules: true,
        },
      },
    ],
  ];

  const plugins = [
    [
      'module-resolver',
      {
        root: ['./src'],
        alias: {
          '@controllers': './src/controllers',
          '@config': './src/config',
          '@libs': './src/libs',
          '@models': './src/models',
          '@routes': './src/routes',
          '@utils': './src/utils',
        },
      },
    ],
  ];

  return {
    presets,
    plugins,
  };
};
